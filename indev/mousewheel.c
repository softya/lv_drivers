/**
 * @file mousewheel.c
 *
 */

/*********************
 *      INCLUDES
 *********************/
#include "mousewheel.h"
#if USE_MOUSEWHEEL

#include "mouse.h"

#ifndef MGBOX_DEBUG
#include <linux/input.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <poll.h>
#include "mgbox-app/mgbox3-cyber-app.h"
#endif

/*********************
 *      DEFINES
 *********************/

/**********************
 *      TYPEDEFS
 **********************/

/**********************
 *  STATIC PROTOTYPES
 **********************/

/**********************
 *  STATIC VARIABLES
 **********************/
static int16_t enc_diff = 0;
static lv_indev_state_t state = LV_INDEV_STATE_REL;

/**********************
 *      MACROS
 **********************/

/**********************
 *   GLOBAL FUNCTIONS
 **********************/

/**
 * Initialize the mousewheel
 */
void mousewheel_init(void)
{
    /*Nothing to init*/
}

/**
 * Get encoder (i.e. mouse wheel) ticks difference and pressed state
 * @param indev_drv pointer to the related input device driver
 * @param data store the read data here
 * @return false: all ticks and button state are handled
 */
bool mousewheel_read2(lv_indev_drv_t * indev_drv, lv_indev_data_t * data)
{
    (void) indev_drv;      /*Unused*/
#ifndef MGBOX_DEBUG

    struct input_event event;
    printf("mousewheel_read()\n");

    if(read(evdev_fd, &event, sizeof(struct input_event)) > 0) {
        printf("mousewheel_read() event type %d code %d value %d\n", event.type, event.code, event.value);

        switch(event.type) {
            case EV_REL:
                // Scroll down (y = -1) means positive encoder turn,
                // so invert it
    #ifdef __EMSCRIPTEN__
                /*Escripten scales it wrong*/
                if(event->wheel.y < 0) enc_diff++;
                if(event->wheel.y > 0) enc_diff--;
    #else
                enc_diff = -event.value;
    #endif
                break;
            case EV_KEY:
                if(event.code == BTN_MIDDLE) {
                    state = event.value;//LV_INDEV_STATE_PR;
                }
                break;
//            case SDL_MOUSEBUTTONUP:
//                if(event->button.button == SDL_BUTTON_MIDDLE) {
//                    state = LV_INDEV_STATE_REL;
//                }
//                break;
            default:
                break;
        }

#endif
    data->state = state;
    data->enc_diff = enc_diff;
    enc_diff = 0;
#ifndef MGBOX_DEBUG
    }
#endif
    return false;       /*No more data to read so return false*/
}

bool mousewheel_read(lv_indev_drv_t * indev_drv, lv_indev_data_t * data)
{
    (void) indev_drv;      /*Unused*/

#ifndef MGBOX_DEBUG
    	int32_t msglen = 0;
    	//read the response
    	MGAPP_INPUT_LOG("mousewheel_read() Entered \n");

//    	memset(buffer, 0, sizeof(message_aux));

    	int timeout, rc;
    	struct pollfd fds[1];
    	int nfds = 1;

    	/*************************************************************/
    	/* Initialize the pollfd structure                           */
    	/*************************************************************/
    	memset(fds, 0, sizeof(fds));

    	/*************************************************************/
    	/* Set up the initial listening socket                       */
    	/*************************************************************/
    	fds[0].fd = evdev_fd;
    	fds[0].events = POLLIN | POLLERR | POLLHUP;
    	/*************************************************************/
    	/* Initialize the timeout to 5 seconds.                      */
    	/* timeout value is based on milliseconds.                   */
    	/*************************************************************/
    	timeout = (10);

    	/*************************************************************/
    	/* waiting for incoming connects or for incoming data   	 */
    	/* on the connected socket.                          		 */
    	/*************************************************************/

    	/*************************************************/
    	/* Call poll() and wait for it to complete.      */
    	/*************************************************/
    	MGAPP_INPUT_LOG("%s() Waiting on poll()...\n", __FUNCTION__);
    	rc = poll(fds, nfds, timeout);

    	/***********************************************************/
    	/* Check to see if the poll call failed.                   */
    	/***********************************************************/
    	if (rc < 0) {
    		MGAPP_INPUT_LOG("%s()  poll() failed", __FUNCTION__);
    		MGAPP_INPUT_LOG("Unsuccessful server response!!!!\n");
//    		return 0;
    		goto finalize;
    	}

    	/*****************************************************/
    	/* Check to see if the timeout has expired.          */
    	/*****************************************************/
    	else if (rc == 0) {
    		MGAPP_INPUT_LOG("%s()  poll() timed out.\n", __FUNCTION__);
    		MGAPP_INPUT_LOG("Unsuccessful server reponse!!!!\n");
//    		return 0;
    		goto finalize;
    	} else {

    		if (fds[0].revents & POLLERR || fds[0].revents & POLLHUP) {
    			MGAPP_INPUT_LOG(
    					"Socket was closed a long time ago...!!!\n\tReconnecting...\n");
//    			return 0;
        		goto finalize;
    		}

    	    struct input_event event;
    	//    MGAPP_INPUT_LOG("mouse_read()\n");

    	    if(msglen = read(evdev_fd, &event, sizeof(struct input_event)) > 0) {
    	        MGAPP_INPUT_LOG("mousewheel_read() %d %d %d\n", event.type, event.code, event.value);

    	        switch(event.type) {
    	            case EV_REL:
    	                // Scroll down (y = -1) means positive encoder turn,
    	                // so invert it
    	    #ifdef __EMSCRIPTEN__
    	                /*Escripten scales it wrong*/
    	                if(event->wheel.y < 0) enc_diff++;
    	                if(event->wheel.y > 0) enc_diff--;
    	    #else
    	                enc_diff = -event.value;
    	    #endif
    	                break;
    	            case EV_KEY:
    	                if(event.code == BTN_MIDDLE) {
    	                    state = event.value;//LV_INDEV_STATE_PR;
    	                }
    	                break;
    	//            case SDL_MOUSEBUTTONUP:
    	//                if(event->button.button == SDL_BUTTON_MIDDLE) {
    	//                    state = LV_INDEV_STATE_REL;
    	//                }
    	//                break;
    	            default:
    	                break;
    	        }


    		if ((msglen > 0) /*&& (NULL != strstr(buff, "HTTP/1.1 200 OK") || 0
    		 == strcmp(buff, "1"))*/) {
    			MGAPP_INPUT_LOG("Good mousewheel_read event!!!!\n");
    		} else {
    			MGAPP_INPUT_LOG(
    					"mouse fd connection was closed...\n");
    		}
    	}

    }
//    MGAPP_INPUT_LOG("mouse_read() evdev_root_x %d evdev_root_y %d \n", evdev_root_x, evdev_root_y);
finalize:

    data->state = state;
    data->enc_diff = enc_diff;
    enc_diff = 0;

#else
//    data->point.x = last_x;
//    data->point.y = last_y;
    data->state = state ? LV_INDEV_STATE_PR : LV_INDEV_STATE_REL;
#endif

    return false;
}

/**
 * It is called periodically from the SDL thread to check mouse wheel state
 * @param event describes the event
 */
void mousewheel_handler(SDL_Event * event)
{
    switch(event->type) {
        case SDL_MOUSEWHEEL:
            // Scroll down (y = -1) means positive encoder turn,
            // so invert it
#ifdef __EMSCRIPTEN__
            /*Escripten scales it wrong*/
            if(event->wheel.y < 0) enc_diff++;
            if(event->wheel.y > 0) enc_diff--;
#else
            enc_diff = -event->wheel.y;
#endif
            break;
        case SDL_MOUSEBUTTONDOWN:
            if(event->button.button == SDL_BUTTON_MIDDLE) {
                state = LV_INDEV_STATE_PR;
            }
            break;
        case SDL_MOUSEBUTTONUP:
            if(event->button.button == SDL_BUTTON_MIDDLE) {
                state = LV_INDEV_STATE_REL;
            }
            break;
        default:
            break;
    }
}

/**********************
 *   STATIC FUNCTIONS
 **********************/

#endif
