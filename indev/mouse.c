/**
 * @file mouse.c
 *
 */

/*********************
 *      INCLUDES
 *********************/
#include "mouse.h"

#ifndef MGBOX_DEBUG
#include "mgbox-app/mgbox3-cyber-app.h"
#include <linux/input.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <poll.h>
#endif

#if USE_MOUSE != 0

/*********************
 *      DEFINES
 *********************/
#ifndef MONITOR_ZOOM
#define MONITOR_ZOOM    1
#endif

/**********************
 *      TYPEDEFS
 **********************/

/**********************
 *  STATIC PROTOTYPES
 **********************/

/**********************
 *  STATIC VARIABLES
 **********************/
static bool left_button_down = false;
static int16_t last_x = 0;
static int16_t last_y = 0;

/**********************
 *      MACROS
 **********************/

/**********************
 *   GLOBAL FUNCTIONS
 **********************/

/**
 * Initialize the mouse
 */
int evdev_fd = -1;
int evdev_root_x = 0;
int evdev_root_y = 0;
lv_indev_state_t evdev_button;

bool mouse_init(const char * pDevice)
{
    int fd;
#ifndef MGBOX_DEBUG
    // Open Mouse device
    fd = open(pDevice, O_RDONLY);
    if(fd == -1)
    {
        printf("mouse_init() ERROR Opening %s\n", pDevice);
        return false;
    }

    printf("mouse_init() Device %s opened successfully\n", pDevice);
    evdev_fd = fd;
    return true;
#endif
}

/**
 * Get the current position and state of the mouse
 * @param indev_drv pointer to the related input device driver
 * @param data store the mouse data here
 * @return false: because the points are not buffered, so no more data to be read
 */
bool mouse_read(lv_indev_drv_t * indev_drv, lv_indev_data_t * data)
{
    (void) indev_drv;      /*Unused*/

#ifndef MGBOX_DEBUG
    	int32_t msglen = 0;
    	//read the response
    	MGAPP_INPUT_LOG("mouse_read() Entered \n");

//    	memset(buffer, 0, sizeof(message_aux));

    	int timeout, rc;
    	struct pollfd fds[1];
    	int nfds = 1;

    	/*************************************************************/
    	/* Initialize the pollfd structure                           */
    	/*************************************************************/
    	memset(fds, 0, sizeof(fds));

    	/*************************************************************/
    	/* Set up the initial listening socket                       */
    	/*************************************************************/
    	fds[0].fd = evdev_fd;
    	fds[0].events = POLLIN | POLLERR | POLLHUP;
    	/*************************************************************/
    	/* Initialize the timeout to 5 seconds.                      */
    	/* timeout value is based on milliseconds.                   */
    	/*************************************************************/
    	timeout = (2);

    	/*************************************************************/
    	/* waiting for incoming connects or for incoming data   	 */
    	/* on the connected socket.                          		 */
    	/*************************************************************/

    	/*************************************************/
    	/* Call poll() and wait for it to complete.      */
    	/*************************************************/
    	MGAPP_INPUT_LOG("%s() Waiting on poll()...\n", __FUNCTION__);
    	rc = poll(fds, nfds, timeout);

    	/***********************************************************/
    	/* Check to see if the poll call failed.                   */
    	/***********************************************************/
		if (rc < 0) {
			MGAPP_INPUT_LOG("%s()  poll() failed", __FUNCTION__);
			MGAPP_INPUT_LOG("probably dev colsed!\n");
			evdev_fd = -1;
			goto finalize;
    	}

    	/*****************************************************/
    	/* Check to see if the timeout has expired.          */
    	/*****************************************************/
    	else if (rc == 0) {
    		MGAPP_INPUT_LOG("%s()  poll() timed out.\n", __FUNCTION__);
//    		return 0;
    		goto finalize;
    	} else {

			if (fds[0].revents & POLLERR || fds[0].revents & POLLHUP) {
				MGAPP_INPUT_LOG(
					"Socket was closed a long time ago...!!!\n\tReconnecting...\n");
				evdev_fd = -1;
				goto finalize;
   		}

    	    struct input_event in;
    	//    MGAPP_INPUT_LOG("mouse_read()\n");

    	    if(msglen = read(evdev_fd, &in, sizeof(struct input_event)) > 0) {
    	        MGAPP_INPUT_LOG("mouse_read() %d %d %d\n", in.type, in.code, in.value);

    	//    	evdev_button = LV_INDEV_STATE_REL;
    	    	if (in.type == EV_REL) {
    	            if (in.code == REL_X)
    	                evdev_root_x += in.value;
    	            else if (in.code == REL_Y)
    	                evdev_root_y += in.value;
    	        } else if (in.type == EV_ABS) {
    	            if (in.code == ABS_X)
    	                evdev_root_x = in.value;
    	            else if (in.code == ABS_Y)
    	                evdev_root_y = in.value;
    	        } else if (in.type == EV_KEY) {
    	            if (in.code == BTN_MOUSE || in.code == BTN_TOUCH) {
    	                if (in.value == 0)
    	                    evdev_button = LV_INDEV_STATE_REL; 	// released
    	                else if (in.value == 1)
    	                    evdev_button = LV_INDEV_STATE_PR;	// pressed (repeated in.value is 2)
    	            }
    	//            if (in.code == BTN_RIGHT) {
    	//                if (in.value == 0)
    	//                    evdev_button = MY_INDEV_STATE_RIGHT_REL; 	// released
    	//                else if (in.value == 1)
    	//                    evdev_button = MY_INDEV_STATE_RIGHT_PR;	// pressed (repeated in.value is 2)
    	//            }
    	        }
    	    }


    		if ((msglen > 0) /*&& (NULL != strstr(buff, "HTTP/1.1 200 OK") || 0
    		 == strcmp(buff, "1"))*/) {
    			MGAPP_INPUT_LOG("Good mouse event!!!!\n");
			} else {
				MGAPP_INPUT_LOG(
						"mouse fd connection was closed...\n");
				evdev_fd = -1;
    		}
    	}


//    MGAPP_INPUT_LOG("mouse_read() evdev_root_x %d evdev_root_y %d \n", evdev_root_x, evdev_root_y);
finalize:
    if (evdev_root_x < 0)
        evdev_root_x = 0;
    if (evdev_root_y < 0)
        evdev_root_y = 0;
    if (evdev_root_x >= LV_HOR_RES)
        evdev_root_x = LV_HOR_RES - 1;
    if (evdev_root_y >= LV_VER_RES)
        evdev_root_y = LV_VER_RES - 1;

    /*Store the collected data*/
    data->point.x = evdev_root_x;
    data->point.y = evdev_root_y;
    data->state = evdev_button;
#else
    data->point.x = last_x;
    data->point.y = last_y;
    data->state = left_button_down ? LV_INDEV_STATE_PR : LV_INDEV_STATE_REL;
#endif

    return false;
}

/**
 * It will be called from the main SDL thread
 */
void mouse_handler(SDL_Event * event)
{
    switch(event->type) {
        case SDL_MOUSEBUTTONUP:
            if(event->button.button == SDL_BUTTON_LEFT)
                left_button_down = false;
            break;
        case SDL_MOUSEBUTTONDOWN:
            if(event->button.button == SDL_BUTTON_LEFT) {
                left_button_down = true;
                last_x = event->motion.x / MONITOR_ZOOM;
                last_y = event->motion.y / MONITOR_ZOOM;
            }
            break;
        case SDL_MOUSEMOTION:
            last_x = event->motion.x / MONITOR_ZOOM;
            last_y = event->motion.y / MONITOR_ZOOM;

            break;
    }

}

/**********************
 *   STATIC FUNCTIONS
 **********************/

#endif
